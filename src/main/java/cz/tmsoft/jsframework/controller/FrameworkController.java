package cz.tmsoft.jsframework.controller;

import cz.tmsoft.jsframework.domain.Framework;
import cz.tmsoft.jsframework.domain.Version;
import cz.tmsoft.jsframework.mapper.FrameworkMapper;
import cz.tmsoft.jsframework.rest.FrameworkRest;
import cz.tmsoft.jsframework.rest.VersionRest;
import cz.tmsoft.jsframework.service.FrameworkService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author tomas.marianek
 */
@RestController
@RequestMapping("/api/v1/frameworks")
public class FrameworkController {

    private final FrameworkService frameworkService;

    public FrameworkController(FrameworkService frameworkService) {
        this.frameworkService = frameworkService;
    }

    @GetMapping
    @Operation(summary = "getAllFrameworks", description = "Return all framework")
    public List<FrameworkRest> getAllFrameworks() {
        List<Framework> retVal = frameworkService.findAll();
        return FrameworkMapper.INSTANCE.mapList(retVal);
    }

    @GetMapping("/{uuid}")
    @Operation(summary = "getFrameworksByUuid", description = "Return framework by specific uuid")
    public FrameworkRest findByUuid(@PathVariable String uuid) {
        Framework retVal = frameworkService.findByUuil(uuid);
        return FrameworkMapper.INSTANCE.map(retVal);
    }

    @PostMapping
    @Operation(summary = "saveFramework", description = "Create new framework")
    public void save(@RequestBody @Valid FrameworkRest frameworkRest) {
        Framework framework = FrameworkMapper.INSTANCE.map(frameworkRest);
        frameworkService.save(framework);
    }

    @PutMapping
    @Operation(summary = "updateFramework", description = "Update framework")
    public void update(@RequestBody @Valid FrameworkRest frameworkRest) {
        Framework framework = FrameworkMapper.INSTANCE.map(frameworkRest);
        frameworkService.update(framework);
    }

    @DeleteMapping("/{uuid}")
    @Operation(summary = "deleteFramework", description = "Delete framework with all version")
    public void delete(@PathVariable String uuid) {
        frameworkService.delete(uuid);
    }

    @GetMapping("/{uuid}/versions")
    @Operation(summary = "getAllVersion", description = "Return all version")
    public List<VersionRest> getAllVersion(@PathVariable String uuid) {
        List<Version> retVal = frameworkService.getAllVersion(uuid);
        return FrameworkMapper.INSTANCE.map(retVal);
    }

    @GetMapping("/{uuid}/versions/{uuidVersion}")
    @Operation(summary = "getVersionByUuid", description = "Return version specific uuid")
    public VersionRest getVersionByUuid(@PathVariable String uuid, @PathVariable String uuidVersion) {
        Version version = frameworkService.getVersionByUuid(uuidVersion);
        return FrameworkMapper.INSTANCE.map(version);
    }

    @PostMapping("/{uuid}/versions")
    @Operation(summary = "saveVersion", description = "Create new version")
    public void saveVersion(@PathVariable String uuid, @RequestBody @Valid  VersionRest versionRest) {
        Version version = FrameworkMapper.INSTANCE.map(versionRest);
        frameworkService.saveVersion(uuid, version);
    }

    @PutMapping("/{uuid}/versions/{uuidVersion}")
    @Operation(summary = "updateVersion", description = "Update version for specific uuid")
    public void updateVersion(@PathVariable String uuid, @PathVariable String uuidVersion, @RequestBody @Valid VersionRest versionRest) {
        Version version = FrameworkMapper.INSTANCE.map(versionRest);
        frameworkService.updateVersion(uuidVersion, version);
    }

    @DeleteMapping("/{uuid}/versions/{uuidVersion}")
    @Operation(summary = "deleteVersion", description = "Delete version for specific uuid")
    public void deleteVersion(@PathVariable String uuid, @PathVariable String uuidVersion) {
        frameworkService.deleteVersion(uuidVersion);
    }
}
