package cz.tmsoft.jsframework.rest;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author tomas.marianek
 */
@Data
public class VersionRest implements Serializable {
    private static final long serialVersionUID = -6093954468765321564L;
    @Schema(description = "Unique identification of version, ", required = true, example = "301e2190-cae9-4882-9e0a-2938b8a5f429")
    @NotEmpty
    @Size(max = 36)
    @Pattern(regexp = "([a-f0-9]{8}(-[a-f0-9]{4}){4}[a-f0-9]{8})")
    private String uuid;
    @Schema(description = "Version number of version, ", required = true, example = "2.0.0")
    @NotEmpty
    @Size(max = 50)
    private String version;
    @Schema(description = "Deprecation date of version, in format yyy-MM-dd", required = true, example = "2018-11-21")
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
    private LocalDate deprecationDate;
    @Schema(description = "Hype lever of version from range 1-10", required = true, example = "1")
    @NotNull
    @Range(min = 1l, max = 10l)
    private Integer hypeLevel;
}
