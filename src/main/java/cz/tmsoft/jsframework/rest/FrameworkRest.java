package cz.tmsoft.jsframework.rest;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author tomas.marianek
 */
@Data
public class FrameworkRest implements Serializable {

    private static final long serialVersionUID = -6241412778507814620L;
    @Schema(description = "Unique identification of framework, ", required = true, example = "301e2190-cae9-4882-9e0a-2938b8a5f429")
    @NotEmpty
    @Size(max = 36)
    @Pattern(regexp = "([a-f0-9]{8}(-[a-f0-9]{4}){4}[a-f0-9]{8})")
    private String uuid;
    @Schema(description = "name of framework", required = true, example = "Angular")
    @NotEmpty
    @Size(max = 50)
    private String name;
    @Schema(description = "description of framework", required = true, example = "Angular web framework")
    @NotEmpty
    @Size(max = 255)
    private String description;
    @Schema(description = "website of framework", required = true, example = "https://angular.io/")
    @NotEmpty
    @Size(max = 255)
    private String url;
    @Schema(description = "Licence of framework", required = true, example = "MIT")
    @NotEmpty
    @Size(max = 50)
    private String licence;
}
