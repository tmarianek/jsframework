package cz.tmsoft.jsframework.mapper;

import cz.tmsoft.jsframework.domain.Framework;
import cz.tmsoft.jsframework.domain.Version;
import cz.tmsoft.jsframework.rest.FrameworkRest;
import cz.tmsoft.jsframework.rest.VersionRest;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @author tomas.marianek
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FrameworkMapper {
    FrameworkMapper INSTANCE = Mappers.getMapper(FrameworkMapper.class);

    List<FrameworkRest> mapList(List<Framework> retVal);

    FrameworkRest map(Framework retVal);

    Framework map(FrameworkRest frameworkRest);

    List<VersionRest> map(List<Version> retVal);

    VersionRest map(Version version);

    Version map(VersionRest versionRest);
}
