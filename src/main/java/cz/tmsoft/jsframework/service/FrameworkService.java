package cz.tmsoft.jsframework.service;

import cz.tmsoft.jsframework.domain.Framework;
import cz.tmsoft.jsframework.domain.Version;
import cz.tmsoft.jsframework.repository.FrameworkRepository;
import cz.tmsoft.jsframework.repository.VersionRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author tomas.marianek
 */
@Service
@Transactional
public class FrameworkService {
    private FrameworkRepository frameworkRepository;
    private VersionRepository versionRepository;

    public FrameworkService(FrameworkRepository frameworkRepository, VersionRepository versionRepository) {
        this.frameworkRepository = frameworkRepository;
        this.versionRepository = versionRepository;
    }

    public List<Framework> findAll() {
        return frameworkRepository.findAll();
    }


    public Framework findByUuil(String uuid) {
        return frameworkRepository.findByUuid(uuid);
    }

    public void save(Framework framework) {
        frameworkRepository.save(framework);
    }

    public void update(Framework framework) {
        Framework dbFramework = frameworkRepository.findByUuid(framework.getUuid());

        if (dbFramework != null) {
            dbFramework.setDescription(framework.getDescription());
            dbFramework.setLicence(framework.getLicence());
            dbFramework.setUrl(framework.getUrl());
            dbFramework.setName(framework.getName());
            frameworkRepository.save(dbFramework);
        }
    }

    public void delete(String uuid) {
        Framework framework = frameworkRepository.findByUuid(uuid);
        versionRepository.deleteByFramework(framework);
        frameworkRepository.deleteByUuid(uuid);
    }

    public List<Version> getAllVersion(String uuid) {
        Framework framework = frameworkRepository.findByUuid(uuid);
        return versionRepository.findByFramework(framework);
    }

    public Version getVersionByUuid(String uuidVersion) {

        return versionRepository.findByUuid(uuidVersion);
    }

    public void saveVersion(String uuid, Version version) {
        Framework framework = frameworkRepository.findByUuid(uuid);
        version.setFramework(framework);
        versionRepository.save(version);
    }

    public void updateVersion(String uuidVersion, Version version) {
        Version dbVersion = versionRepository.findByUuid(uuidVersion);

        if(dbVersion != null ){
            dbVersion.setVersion(version.getVersion());
            dbVersion.setHypeLevel(version.getHypeLevel());
            dbVersion.setDeprecationDate(version.getDeprecationDate());
            versionRepository.save(dbVersion);
        }
    }

    public void deleteVersion(String uuidVersion) {
        versionRepository.deleteByUuid(uuidVersion);
    }
}
