package cz.tmsoft.jsframework.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author tomas.marianek
 */
@Data
@Entity
@Table(name = "Framework")
public class Framework implements Serializable {
    private static final long serialVersionUID = 1809324309150359284L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(length = 36)
    private String uuid;
    @Column(length = 50)
    private String name;
    private String description;
    private String url;
    @Column(length = 50)
    private String licence;
    @OneToMany(mappedBy = "version", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Version> versions;
}
