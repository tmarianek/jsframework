package cz.tmsoft.jsframework.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author tomas.marianek
 */
@Entity
@Data
@Table(name = "Version")
public class Version implements Serializable {
    private static final long serialVersionUID = 7174145069614730717L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(length = 36)
    private String uuid;
    @Column(length = 50)
    private String version;
    private LocalDate deprecationDate;
    private Integer hypeLevel;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "id_framework", nullable = false)
    private Framework framework;
}
