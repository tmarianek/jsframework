package cz.tmsoft.jsframework.repository;

import cz.tmsoft.jsframework.domain.Framework;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author tomas.marianek
 */
public interface FrameworkRepository extends JpaRepository<Framework, Long> {

    Framework findByUuid(String uuid);

    void deleteByUuid(String uuid);
}
