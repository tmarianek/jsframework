package cz.tmsoft.jsframework.repository;

import cz.tmsoft.jsframework.domain.Framework;
import cz.tmsoft.jsframework.domain.Version;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author tomas.marianek
 */
public interface VersionRepository extends JpaRepository<Version, Long> {


    List<Version> findByFramework(Framework framework);

    void deleteByFramework(Framework framework);

    Version findByUuid(String uuidVersion);

    void deleteByUuid(String uuidVersion);
}
