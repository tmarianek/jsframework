package cz.tmsoft.jsframework.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author tomas.marianek
 */
@Configuration
@OpenAPIDefinition
public class OpenApiConfig {

    @Bean
    public OpenAPI jsFrameworkOpenAPI() {
        return new OpenAPI()
                .info(
                        new Info()
                                .title("Javascript framework API")
                                .description("Javascript framework application")
                                .version("0.0.1")
                                .contact(new Contact().name("Tomas Marianek").email("tomas.marianek@prorocketeers.com"))
                );
    }

    @Bean
    public GroupedOpenApi jsFrameworkGroupApi() {
        return GroupedOpenApi.builder()
                .group("JSFRAMEWORK")
                .pathsToMatch("/api/v1/**")
                .packagesToScan("cz.tmsoft.jsframework.controller")
                .build();
    }
}
