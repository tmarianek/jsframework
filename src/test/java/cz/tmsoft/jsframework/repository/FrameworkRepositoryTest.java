package cz.tmsoft.jsframework.repository;

import cz.tmsoft.jsframework.domain.Framework;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author tomas.marianek
 */
@DataJpaTest
class FrameworkRepositoryTest {

    @Autowired
    private FrameworkRepository frameworkRepository;

    @Test
    void findByUuid() {
        String uuid = UUID.randomUUID().toString();
        Framework framework = new Framework();
        framework.setName("Angular");
        framework.setUuid(uuid);
        framework.setLicence("MIT");
        framework.setUrl("https://angular.io/");
        framework.setDescription("Angular (commonly referred to as \"Angular 2+\" or \"Angular CLI\") is a TypeScript-based free and open-source web application framework led by the Angular Team at Google and by a community of individuals and corporations");
        frameworkRepository.save(framework);

        Framework retVal = frameworkRepository.findByUuid(uuid);
        Assertions.assertNotNull(retVal);
    }

    @Test
    void findByUuidNotExist(){
        Framework nesmysl = frameworkRepository.findByUuid("nesmysl");

        Assertions.assertNull(nesmysl);
    }

    @Test
    public void deleteByUuid(){
        List<Framework> all = frameworkRepository.findAll();
        String uuid = UUID.randomUUID().toString();
        Framework framework = new Framework();
        framework.setName("Angular");
        framework.setUuid(uuid);
        framework.setLicence("MIT");
        framework.setUrl("https://angular.io/");
        framework.setDescription("Angular (commonly referred to as \"Angular 2+\" or \"Angular CLI\") is a TypeScript-based free and open-source web application framework led by the Angular Team at Google and by a community of individuals and corporations");
        frameworkRepository.save(framework);

        frameworkRepository.deleteByUuid(uuid);

        Framework retVal = frameworkRepository.findByUuid(uuid);

        Assertions.assertNull(retVal);
    }
}
