package cz.tmsoft.jsframework.repository;

import cz.tmsoft.jsframework.domain.Framework;
import cz.tmsoft.jsframework.domain.Version;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

/**
 * @author tomas.marianek
 */
@DataJpaTest
class VersionRepositoryTest {

    @Autowired
    private VersionRepository versionRepository;
    @Autowired
    private FrameworkRepository frameworkRepository;

    @Test
    void findByFramework() {
        Framework framework = new Framework();
        framework.setName("Angular");
        framework.setUuid(UUID.randomUUID().toString());
        framework.setLicence("MIT");
        framework.setUrl("https://angular.io/");
        framework.setDescription("Angular (commonly referred to as \"Angular 2+\" or \"Angular CLI\") is a TypeScript-based free and open-source web application framework led by the Angular Team at Google and by a community of individuals and corporations");
        frameworkRepository.save(framework);

        Version version = new Version();
        version.setVersion("1.0.0");
        version.setDeprecationDate(LocalDate.now());
        version.setUuid(UUID.randomUUID().toString());
        version.setHypeLevel(5);
        version.setFramework(framework);

        versionRepository.save(version);

        List<Version> retVal = versionRepository.findByFramework(framework);

        Assertions.assertNotNull(retVal);
        Assertions.assertTrue(retVal.size() > 0);
    }

    @Test
    public void deleteByFramework(){
        Framework framework = new Framework();
        framework.setName("Angular");
        framework.setUuid(UUID.randomUUID().toString());
        framework.setLicence("MIT");
        framework.setUrl("https://angular.io/");
        framework.setDescription("Angular (commonly referred to as \"Angular 2+\" or \"Angular CLI\") is a TypeScript-based free and open-source web application framework led by the Angular Team at Google and by a community of individuals and corporations");
        frameworkRepository.save(framework);

        Version version = new Version();
        version.setVersion("1.0.0");
        version.setDeprecationDate(LocalDate.now());
        version.setUuid(UUID.randomUUID().toString());
        version.setHypeLevel(5);
        version.setFramework(framework);

        versionRepository.save(version);

        versionRepository.deleteByFramework(framework);

        List<Version> byFramework = versionRepository.findByFramework(framework);

        Assertions.assertTrue(byFramework.size() == 0);
    }
}
