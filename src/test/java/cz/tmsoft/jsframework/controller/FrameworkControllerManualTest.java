package cz.tmsoft.jsframework.controller;

import cz.tmsoft.jsframework.rest.FrameworkRest;
import cz.tmsoft.jsframework.rest.VersionRest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author tomas.marianek
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class FrameworkControllerManualTest {
    // bind the above RANDOM_PORT
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void testFramework() {
        String url = "http://localhost:" + port + "/api/v1/frameworks";
        String uuid = UUID.randomUUID().toString();

        //test get all
        ResponseEntity<FrameworkRest[]> responseAll = restTemplate.getForEntity(url, FrameworkRest[].class);
        assertNotNull(responseAll);
        assertTrue(responseAll.getStatusCode().is2xxSuccessful());

        //test save
        FrameworkRest frameworkRest = new FrameworkRest();
        frameworkRest.setName("Angular");
        frameworkRest.setDescription("Angular web framework");
        frameworkRest.setUrl("https://angular.io/");
        frameworkRest.setLicence("MIT");
        frameworkRest.setUuid(uuid);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");

        HttpEntity<FrameworkRest> requestSave = new HttpEntity<>(frameworkRest, headers);

        ResponseEntity<Void> responseSave = restTemplate.postForEntity(url, requestSave, Void.class);

        assertNotNull(responseSave);
        assertTrue(responseSave.getStatusCode().is2xxSuccessful());

        //test get by uuid
        ResponseEntity<FrameworkRest> responseByUuid = restTemplate.getForEntity(url + "/" + uuid, FrameworkRest.class);

        assertNotNull(responseByUuid);
        assertTrue(responseByUuid.getStatusCode().is2xxSuccessful());
        assertNotNull(responseByUuid.getBody());

        //test update
        frameworkRest.setLicence("MITLI");

        ResponseEntity<Void> responsePut = restTemplate.exchange(url, HttpMethod.PUT, requestSave, Void.class);

        assertNotNull(responsePut);
        assertTrue(responsePut.getStatusCode().is2xxSuccessful());

        //test it is update element ok
        ResponseEntity<FrameworkRest> responseUpdate = restTemplate.getForEntity(url + "/" + uuid, FrameworkRest.class);

        assertNotNull(responseUpdate);
        assertNotNull(responseUpdate.getBody());
        assertEquals("MITLI", responseUpdate.getBody().getLicence());

        //test delete
        restTemplate.delete(url + "/" + uuid);

        ResponseEntity<FrameworkRest> responseDelete = restTemplate.getForEntity(url + "/" + uuid, FrameworkRest.class);

        assertNotNull(responseDelete);
        assertTrue(responseDelete.getStatusCode().is2xxSuccessful());
        assertNull(responseDelete.getBody());

    }

    @Test
    void testVersion() {
        String urlFramework = "http://localhost:" + port + "/api/v1/frameworks";
        String uuidFramework = UUID.randomUUID().toString();
        String urlVersion = urlFramework + "/" + uuidFramework +"/versions";
        String uuidVersion = UUID.randomUUID().toString();

        //test save
        FrameworkRest frameworkRest = new FrameworkRest();
        frameworkRest.setName("Angular");
        frameworkRest.setDescription("Angular web framework");
        frameworkRest.setUrl("https://angular.io/");
        frameworkRest.setLicence("MIT");
        frameworkRest.setUuid(uuidFramework);
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");

        HttpEntity<FrameworkRest> requestSaveFramework = new HttpEntity<>(frameworkRest, headers);

        ResponseEntity<Void> responseSaveFramework = restTemplate.postForEntity(urlFramework, requestSaveFramework, Void.class);

        assertNotNull(responseSaveFramework);
        assertTrue(responseSaveFramework.getStatusCode().is2xxSuccessful());

        // test get all
        ResponseEntity<VersionRest[]> responseAll = restTemplate.getForEntity(urlVersion, VersionRest[].class);
        assertNotNull(responseAll);
        assertTrue(responseAll.getStatusCode().is2xxSuccessful());


        //Test save
        VersionRest versionRest = new VersionRest();
        versionRest.setVersion("2");
        versionRest.setUuid(uuidVersion);
        versionRest.setDeprecationDate(LocalDate.now());
        versionRest.setHypeLevel(2);

        HttpEntity<VersionRest> requestSave = new HttpEntity<>(versionRest, headers);

        ResponseEntity<Void> responseSave = restTemplate.postForEntity(urlVersion, requestSave, Void.class);
        assertNotNull(responseSave);
        assertTrue(responseSave.getStatusCode().is2xxSuccessful());

        //test get by uuid
        ResponseEntity<VersionRest> responseByUuid = restTemplate.getForEntity(urlVersion + "/" + uuidVersion, VersionRest.class);

        assertNotNull(responseByUuid);
        assertTrue(responseByUuid.getStatusCode().is2xxSuccessful());

        //test update
        versionRest.setVersion("2.0.0");

        ResponseEntity<Void> responsePut = restTemplate.exchange(urlVersion + "/" + uuidVersion, HttpMethod.PUT, requestSave, Void.class);

        assertNotNull(responsePut);
        assertTrue(responsePut.getStatusCode().is2xxSuccessful());

        //test it is update element ok
        ResponseEntity<VersionRest> responseUpdate = restTemplate.getForEntity(urlVersion + "/" + uuidVersion, VersionRest.class);

        assertNotNull(responseUpdate);
        assertNotNull(responseUpdate.getBody());
        assertEquals("2.0.0", responseUpdate.getBody().getVersion());

        //test delete
        restTemplate.delete(urlVersion + "/" + uuidVersion);

        ResponseEntity<VersionRest> responseDelete = restTemplate.getForEntity(urlVersion + "/" + uuidVersion, VersionRest.class);

        assertNotNull(responseDelete);
        assertTrue(responseDelete.getStatusCode().is2xxSuccessful());
        assertNull(responseDelete.getBody());

        //test delete
        restTemplate.delete(urlFramework + "/" + uuidFramework);
    }
}
