# jsframework

### run
```
./gradlew bootRun
```

### test
manualni test
```
cz.tmsoft.jsframework.controller.FrameworkControllerManualTest 
```

### Open API documentation

```
http://localhost:8085/swagger-ui/index.html
```
